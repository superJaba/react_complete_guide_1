/* eslint-disable */
import React, {useState} from "react";
import './ExpenseForm.css';

const ExpenseForm = (props) => {
    // ------------THIS IS IN CASE TO HAVE useState FOR EVERY onChange INDEPENDENT ------------
    const [enteredTitle, setEnteredTitle] = useState('');
    const [enteredAmount, setEnteredAmount] = useState('');
    const [enteredDate, setEnteredDate] = useState('');

    // ------------ ONE OBJECT FOR EVERY INPUT TO STORE THE VALUE & USE THE PROPER ONE CODE FOR EACH EVENT HANDLER ------------
    // const [userInput, setUserInput] = useState({
    //     enteredTitle: '',
    //     enteredAmount: '',
    //     enteredDate: ''
    // });

    // ------------ function to handle user input triggered on field change ------------
    const titleChangeHandler = (event) => {
        // ------------ THIS IS IN CASE TO HAVE useState FOR EVERY onChange INDEPENDENT ------------
        setEnteredTitle(event.target.value);
        // ------------ NOT A GOOD PRACTISE ------------
        // setUserInput({
        //     ...userInput,
        //     enteredTitle: event.target.value,
        // });

        // ------------ ANOTHER APPROACH & PROPER ONE FOR LATEST SNAPSHOT ------------
        // setUserInput((prevState) => {
        //     return {...prevState, enteredTitle: event.target.value};
        // });
    };
    // ------------ function to handle user input triggered on field change ------------
    const amountChangeHandler = (event) => {
        // ------------ THIS IS IN CASE TO HAVE useState FOR EVERY onChange INDEPENDENT ------------
        setEnteredAmount(event.target.value);
        // ------------ NOT A GOOD PRACTISE ------------
        // setUserInput({
        //     ...userInput,
        //     enteredAmount: event.target.value
        // });

        // ------------ ANOTHER APPROACH & PROPER ONE FOR LATEST SNAPSHOT ------------
        // setUserInput((prevState) => {
        //     return {...prevState, enteredAmount: event.target.value};
        // });
    };
    // ------------ function to handle user input triggered on field change ------------
    const dateChangeHandler = (event) => {
        // ------------ THIS IS IN CASE TO HAVE useState FOR EVERY onChange INDEPENDENT ------------
        setEnteredDate(event.target.value);
        // ------------ NOT A GOOD PRACTISE ------------
        // setUserInput({
        //     ...userInput,
        //     enteredDate: event.target.value
        // });

        // ------------ ANOTHER APPROACH & PROPER ONE FOR LATEST SNAPSHOT ------------
        // setUserInput((prevState) => {
        //     return {...prevState, enteredDate: event.target.value};
        // });
    };

    const submitHandler = (event) => {
        event.preventDefault(); // ------------ not reloading the page in browser!!! ------------

        const expenseData = {
            titleInput: enteredTitle,
            amountInput: +enteredAmount,
            dateInput: new Date(enteredDate)
        };

        // needed to extract data to parent
        props.onSaveExpenseDataInExpenseForm(expenseData);
        setEnteredTitle('');
        setEnteredAmount('');
        setEnteredDate('');
    };

    return <form onSubmit={submitHandler}>
        <div className='new-expense__controls'>
            <div className='new-expense__control'>
                <label>Title</label>
                <input type="text"
                       value={enteredTitle}
                       onChange={titleChangeHandler}/> {/*change in field trigger function pasted as
                       parameter (onChange={titleChangeHandler})*/}
            </div>
            <div className='new-expense__control'>
                <label>Amount</label>
                <input type="number"
                       min='0.01'
                       step='0.01'
                       value={enteredAmount}
                       onChange={amountChangeHandler}/> {/*change in field trigger function pasted as
                       parameter (onChange={amountChangeHandler})*/}
            </div>
            <div className='new-expense__control'>
                <label>Date</label>
                <input type="date"
                       min='2019-01-01'
                       max='2022-12-31'
                       value={enteredDate}
                       onChange={dateChangeHandler}/> {/*change in field trigger function pasted as
                       parameter (onChange={dateChangeHandler})*/}
            </div>
        </div>
        <div className='new-expense__actions'>
            <button type='button' onClick={props.onCancel}>Cancel</button>
            <button type='submit'>Add Expense</button>
        </div>
    </form>
};

export default ExpenseForm;