import './NewExpense.css';
import React, {useState} from "react";
import ExpenseForm from "./ExpenseForm";

const NewExpense = (props) => {

    const [isEditing, setIsStartEditing] = useState(false);

    const saveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        };
        props.onAddExpense(expenseData); //sending data to App.addExpenseHandler() by <NewExpense onAddExpense={addExpenseHandler}/>
        setIsStartEditing(false);
    };

    const startEditingHandler = () => {
        setIsStartEditing(true);
    }

    const stopEditingHandler = () => {
        setIsStartEditing(false);
    }

    return (
        <div className='new-expense'>
            {!isEditing && <button onClick={startEditingHandler}>Add New Expense</button>}
            {isEditing && <ExpenseForm
                onSaveExpenseDataInExpenseForm={saveExpenseDataHandler}
                onCancel={stopEditingHandler}/>} {/*GETTING DATA FROM CHILD*/}
        </div>
    );
}

export default NewExpense;